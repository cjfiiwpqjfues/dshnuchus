var fs = require('fs');
var targetPath = "/usr/local/etc/xray/config.json";
var finalPath = "/root/serviceConnectionCode";

var domain = "";

updateConfig();

function updateConfig() {

    fs.readFile(targetPath, function(error, configData) {
        if (error) {
            console.log('讀取失敗');
        } else {
            var jsonConfig = JSON.parse(configData.toString());
            jsonConfig.inbounds[1].settings.clients[0].alterId = 444;
            fs.writeFile(targetPath, JSON.stringify(jsonConfig, null, 4), function(error) {

                if (error) {
                    console.log('寫入失敗');
                } else {
                    console.log('寫入成功');
                    getConfig();
                }

            });

        }
    });

}

function getConfig() {

    fs.readFile(targetPath, function(error, configData) {
        if (error) {
            console.log('讀取失敗');
        } else {
            setConfig(configData.toString());
        }
    });

}

function setConfig(rawConfig) {

    var config = JSON.parse(rawConfig);

    var newConfig = {
        "v": "2",
        "ps": domain,
        "add": domain,
        "port": config.inbounds[0].port.toString(),
        "id": config.inbounds[1].settings.clients[0].id,
        "aid": config.inbounds[1].settings.clients[0].alterId.toString(),
        "net": "ws",
        "type": "none",
        "host": domain,
        "path": config.inbounds[0].settings.fallbacks[0].path,
        "tls": "tls"
    }

    if (domain == "2") {
        console.log('Domain Fail');
    } else {
        fs.readFile("/usr/local/nginx/certs/" + domain + ".key", function(error, configData) {
            if (error) {
                console.log('Nginx Fail');
            } else {
                fs.readFile("/usr/local/nginx/html/" + domain + "/config/config.php", function(error1, configData1) {
                    if (error1) {
                        console.log('Nginx Fail');
                    } else {
                        writeConfig(JSON.stringify(newConfig, null, 4));
                    }
                });
            }
        });
    }
}

function writeConfig(finalConfig) {

    fs.writeFile(finalPath, finalConfig, function(error) {

        if (error) {
            console.log('寫入失敗');
        } else {
            console.log('寫入成功')
        }

    });

}
