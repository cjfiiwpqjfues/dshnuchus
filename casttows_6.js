var fs = require('fs');
var targetPath = "/usr/local/etc/v2ray/config.json";
var finalPath = "/root/serviceConnectionCode";

var domain = "";

getConfig();

function getConfig() {

    fs.readFile(targetPath, function(error, configData) {
        if (error) {
            console.log('讀取失敗');
        } else {
            setConfig(configData.toString());
        }
    });

}

function setConfig(rawConfig) {

    var config = JSON.parse(rawConfig);

    var newConfig = {
        "v": "2",
        "ps": "vless_kirin_"+domain,
        "add": domain,
        "port": config.inbounds[0].port.toString(),
        "id": config.inbounds[1].settings.clients[0].id,
        "aid": "",
        "net": "ws",
        "type": "none",
        "host": domain,
        "path": config.inbounds[0].settings.fallbacks[0].path,
        "tls": "tls"
    }

    if (domain == "2") {
        console.log('Domain Fail');
    } else {
        fs.readFile("/etc/nginx/certs/"+domain+".key", function(error, configData) {
            if (error) {
                console.log('Nginx Fail');
            } else {
                writeConfig(JSON.stringify(newConfig, null, 4));
            }
        });
    }
}

function writeConfig(finalConfig) {

    fs.writeFile(finalPath, finalConfig, function(error) {

        if (error) {
            console.log('寫入失敗');
        } else {
            console.log('寫入成功')
        }

    });

}
