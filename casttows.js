var fs = require('fs');
var targetPath = "/etc/v2ray-agent/xray/conf/05_VMess_WS_inbounds.json";
var finalPath = "/usr/local/vmess_qr.json";

getConfig();

function getConfig() {

    fs.readFile(targetPath, function(error, data) {
        if (error) {
            console.log('讀取失敗');
        } else {
            setConfig(data.toString());
        }
    });

}

function setConfig(rawConfig) {

    var config = JSON.parse(rawConfig);

    var newConfig = {
        "port": "443",
        "ps": config.inbounds[0].settings.clients[0].email.replace('_vmess_ws', ''),
        "tls": "tls",
        "id": config.inbounds[0].settings.clients[0].id,
        "aid": "1",
        "v": "2",
        "host": config.inbounds[0].settings.clients[0].email.replace('_vmess_ws', ''),
        "type": "none",
        "path": config.inbounds[0].streamSettings.wsSettings.path,
        "net": "ws",
        "add": config.inbounds[0].settings.clients[0].add,
        "allowInsecure": 0,
        "method": "none",
        "peer": config.inbounds[0].settings.clients[0].email.replace('_vmess_ws', '')
    }

    writeConfig(JSON.stringify(newConfig, null, 4));
}

function writeConfig(finalConfig) {

    fs.writeFile(finalPath, finalConfig, function(error) {

        if (error) {
            console.log('寫入失敗');
        } else {
            console.log('寫入成功')
        }

    });

}
