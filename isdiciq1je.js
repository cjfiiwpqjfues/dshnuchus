var fs = require('fs');
const {
    v4: uuidv4
} = require('uuid');

var configJsonPath = "/etc/v2ray/config.json";
var vmessQrJsonPath = "/usr/local/vmess_qr.json";

var args = process.argv;

writeDate();

function writeDate() {

    var uuid = uuidv4();

    var configJson = {
        log: {
            access: "/var/log/v2ray/access.log",
            error: "/var/log/v2ray/error.log",
            loglevel: "warning"
        },
        inbounds: [{
            port: 443,
            protocol: "vmess",
            settings: {
                clients: [{
                    id: uuid,
                    alterId: 444
                }]
            },
            streamSettings: {
                network: "tcp",
                security: "tls",
                tlsSettings: {
                    certificates: [{
                        certificateFile: "/etc/v2ray/v2ray.crt",
                        keyFile: "/etc/v2ray/v2ray.key"
                    }]
                }
            },
            sniffing: {
                enabled: true,
                destOverride: [
                    "http",
                    "tls"
                ]
            }
        }],
        outbounds: [{
                protocol: "freedom",
                settings: {}
            },
            {
                protocol: "blackhole",
                settings: {},
                tag: "block"
            }
        ],
        dns: {
            servers: [
                "https+local://1.1.1.1/dns-query",
                "1.1.1.1",
                "1.0.0.1",
                "8.8.8.8",
                "8.8.4.4",
                "localhost"
            ]
        },
        routing: {
            domainStrategy: "AsIs",
            rules: [{
                type: "field",
                outboundTag: "block",
                protocol: [
                    "bittorrent"
                ]
            }]
        }
    };

    fs.writeFile(configJsonPath, JSON.stringify(configJson, null, 4), function(error) {

    })

    var vmessQrJson = {
        v: "2",
        host: "",
        ps: args[2],
        add: args[2],
        port: "443",
        id: uuid,
        aid: "444",
        net: "tcp",
        type: "none",
        tls: "tls",
        path: ""
    };

    fs.writeFile(vmessQrJsonPath, JSON.stringify(vmessQrJson, null, 4), function(error) {

    })

}