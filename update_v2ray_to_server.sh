#!/bin/bash

get_ip(){
    local IP=$( ip addr | egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | egrep -v "^192\.168|^172\.1[6-9]\.|^172\.2[0-9]\.|^172\.3[0-2]\.|^10\.|^127\.|^255\.|^0\." | head -n 1 )
    [ -z ${IP} ] && IP=$( wget -qO- -t1 -T2 ipv4.icanhazip.com )
    [ -z ${IP} ] && IP=$( wget -qO- -t1 -T2 ipinfo.io/ip )
    [ ! -z ${IP} ] && echo ${IP} || echo
}

myIP=$(get_ip)
ipKey=$myIP
ipKey=${ipKey//./_}
ipKey=$ipKey"_vmess"

vmessUrlRaw=$(/usr/local/sbin/v2ray url | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g")

if [[ -n "$vmessUrlRaw" ]]; then

    vmessUrl="vmess://${vmessUrlRaw#*vmess://}"
    connectValue="\\\"$vmessUrl\\\"","//$myIP"

    echo $vmessUrl

    curl -X PATCH -d '{ "'$ipKey'": "'$connectValue'"}' \
    'https://ndnshvisjdimsdiw.firebaseio.com/connect_list.json'
  
fi