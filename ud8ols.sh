#!/bin/bash

serviceConnectionCodePath="/root/serviceConnectionCode"
serviceConfigPath="/usr/local/etc/xray/config.json"

ip=$(curl -s https://ipinfo.io/ip)
	[[ -z $ip ]] && ip=$(curl -s https://api.ip.sb/ip)
	[[ -z $ip ]] && ip=$(curl -s https://api.ipify.org)
	[[ -z $ip ]] && ip=$(curl -s https://ip.seeip.org)
	[[ -z $ip ]] && ip=$(curl -s https://ifconfig.co/ip)
	[[ -z $ip ]] && ip=$(curl -s https://api.myip.com | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}")
	[[ -z $ip ]] && ip=$(curl -s icanhazip.com)
	[[ -z $ip ]] && ip=$(curl -s myip.ipip.net | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}")

uuid=$(cat /proc/sys/kernel/random/uuid)
port=$(shuf -i1024-65535 -n1)

echo "{\"log\":{\"loglevel\":\"warning\"},\"inbounds\":[{\"port\":1111,\"protocol\":\"vmess\",\"settings\":{\"clients\":[{\"id\":\"2222\",\"alterId\":0}]},\"streamSettings\":{\"network\":\"ws\"},\"sniffing\":{\"enabled\":true,\"destOverride\":[\"http\",\"tls\"]}}],\"outbounds\":[{			\"sendThrough\":\"3333\",\"protocol\":\"freedom\",\"settings\":{}},{\"protocol\":\"blackhole\",\"settings\":{},\"tag\":\"blocked\"}],\"dns\":{\"servers\":[\"https+local://cloudflare-dns.com/dns-query\",\"1.1.1.1\",\"1.0.0.1\",\"8.8.8.8\",\"8.8.4.4\",\"localhost\"]},\"routing\":{\"domainStrategy\":\"AsIs\",\"rules\":[{\"type\":\"field\",\"protocol\":[\"bittorrent\"],\"outboundTag\":\"blocked\"}]}}" > ${serviceConfigPath}

sed -i 's/1111/'${port}'/g' ${serviceConfigPath}
sed -i 's/2222/'${uuid}'/g' ${serviceConfigPath}
sed -i 's/3333/'${ip}'/g' ${serviceConfigPath}

ipKey=$ip
ip=\"${ip}\"
uuid=\"${uuid}\"
port=\"${port}\"

echo -n "{\"v\":\"2\",\"ps\":${ip},\"add\":${ip},\"port\":${port},\"id\":${uuid},\"aid\":\"0\",\"net\":\"ws\",\"type\":\"none\",\"host\":\"\",\"path\":\"\",\"tls\":\"\"}" > ${serviceConnectionCodePath}

/root/restart_all_v2ray_service_version_1.sh

connectValue=$(cat /root/serviceConnectionCode | base64 -w 0)

vmessUrl="vmess://${connectValue}"
# echo $vmessUrl


ipKey=${ipKey//./_}
/usr/bin/node /root/isdncsdcjsidf/qpwicmija.js $ipKey $vmessUrl
