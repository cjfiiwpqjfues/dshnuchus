var fs = require('fs');
var targetPath = "/usr/local/etc/xray/config.json";
var finalPath = "/usr/local/vmess_qr.json";
var domainPath = "/usr/local/etc/xray/domain";

var domain = "";

getConfig();

function getConfig() {

    fs.readFile(targetPath, function(error, configData) {
        if (error) {
            console.log('讀取失敗');
        } else {
            fs.readFile(domainPath, function(error, domainData) {
                if (error) {
                    console.log('讀取失敗');
                } else {
                    domain = domainData.toString().replace('\n', '');
                    setConfig(configData.toString());
                }
            });
        }
    });

}

function setConfig(rawConfig) {

    var config = JSON.parse(rawConfig);
    var args = process.argv;
    
    var newConfig = {
        "v": "2",
        "ps": "vless_nginx_"+domain,
        "add": domain,
        "port": args[2],
        "id": config.inbounds[0].settings.clients[0].id,
        "aid": "",
        "net": "ws",
        "type": "none",
        "host": domain,
        "path": config.inbounds[0].streamSettings.wsSettings.path,
        "tls": "tls"
    }

    if (domain == "2") {
        console.log('Domain Fail');
    } else {
        fs.readFile("/ssl/xray.key", function(error, configData) {
            if (error) {
                console.log('Nginx Fail');
            } else {
                writeConfig(JSON.stringify(newConfig, null, 4));
            }
        });
    }
}

function writeConfig(finalConfig) {

    fs.writeFile(finalPath, finalConfig, function(error) {

        if (error) {
            console.log('寫入失敗');
        } else {
            console.log('寫入成功')
        }

    });

}
