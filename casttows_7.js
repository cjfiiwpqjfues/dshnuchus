var fs = require('fs');
var targetPath = "/usr/local/vmess_qr.json";
var finalPath = "/root/serviceConnectionCode";

getConfig();

function getConfig() {

    fs.readFile(targetPath, function(error, configData) {
        if (error) {
            console.log('讀取失敗');
        } else {
            fs.readFile("/data/v2ray.key", function(error, sslData) {
                if (error) {
                    console.log('Nginx Fail');
                } else {
                    writeConfig(configData);
                }
            });
        }
    });

}

function writeConfig(finalConfig) {

    fs.writeFile(finalPath, finalConfig, function(error) {

        if (error) {
            console.log('寫入失敗');
        } else {
            console.log('寫入成功')
        }

    });

}
