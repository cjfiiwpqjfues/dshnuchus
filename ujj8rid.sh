#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

Test_link="www.189.cn
uac.10010.com
www.10086.cn"
# Test_link="www.10086.cn"
Test_UA="Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36
Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36 LBBROWSER
Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134
Mozilla/5.0 (Linux; Android 8.0.0; MHA-AL00 Build/HUAWEIMHA-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Mobile Safari/537.36
Mozilla/5.0 (Linux; Android 7.0; LG-H850 Build/NRD90U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36
Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
Mozilla/5.0 (iPhone; CPU iPhone OS 12_0_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1
Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_2 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) CriOS/30.0.1599.12 Mobile/11A501 Safari/8536.25 MicroMessenger/6.1.0
Mozilla/5.0 (iPad; CPU OS 12_0_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1"

rand(){
	rand_min=$1
	rand_max=$(($2-$rand_min+1))
	rand_num=$(date +%s%N)
	echo $(($rand_num%$rand_max+$rand_min))
}
Test(){
	Detailed_output="${1}"
	all_status_num="0"
	Return_status_debug=""
	status_num_debug=""
	Test_total=$(echo "${Test_link}"|wc -l)
	for((integer = 1; integer <= ${Test_total}; integer++))
	do
		UA_num=$(rand 1 12)
		UA=$(echo "${Test_UA}"|sed -n "${UA_num}p")
		now_URL=$(echo "${Test_link}"|sed -n "${integer}p")
		nc -zvw3 "${now_URL}" 80 && return_code=0 || return_code=1

		# wget --spider -nv -t2 -T5 -4 -U "${UA}" "${now_URL}" -o "http_code.tmp"
		# #wget --spider -nv -t2 -T5 -U "${UA}" "${now_URL}" &> /dev/null
		# return_code=$(echo $?)
		# #cat "http_code.tmp"
		# #Return_status=$(cat "http_code.tmp"|sed -n '$p'|awk '{print $NF}')
		# Return_status_debug="${Return_status_debug} | $(cat "http_code.tmp")"
		# return_code_debug="${return_code_debug} | ${return_code}"
		# #Return_status_debug="${Return_status_debug} | ${return_code}"
		# #echo "${Return_status}"
		# rm -rf "http_code.tmp"

		if [[ "${return_code}" == "0" ]]; then
			status_num="1"
			status_num_debug="${status_num_debug} | ${status_num}"
			[[ "${Detailed_output}" == "1" ]] && echo -e "pass"
		else
			status_num="0"
			status_num_debug="${status_num_debug} | ${status_num}"
			[[ "${Detailed_output}" == "1" ]] && echo -e "no"
		fi
		all_status_num=$(echo $((${all_status_num}+${status_num})))
	done
}
Manual_detection(){
	Test "1"
	if [[ "${all_status_num}" == "${Test_total}" ]]; then
		echo -e "good"
		update_server
	elif [[ "${all_status_num}" == "0" ]]; then
		echo -e "bad"
		# update_server
	else
		echo -e "not good"
		update_server
	fi
}
get_ip(){
    local IP=$( ip addr | egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | egrep -v "^192\.168|^172\.1[6-9]\.|^172\.2[0-9]\.|^172\.3[0-2]\.|^10\.|^127\.|^255\.|^0\." | head -n 1 )
    [ -z ${IP} ] && IP=$( wget -qO- -t1 -T2 ipv4.icanhazip.com )
    [ -z ${IP} ] && IP=$( wget -qO- -t1 -T2 ipinfo.io/ip )
    [ ! -z ${IP} ] && echo ${IP} || echo
}
update_server(){
		myIP=$(get_ip)
		ipKey=$myIP
		ipKey=${ipKey//./_}
		vmessUrlRaw=$(/usr/local/sbin/v2ray url | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g")
		vmessUrl="vmess://${vmessUrlRaw#*vmess://}"
		connectValue="\\\"$vmessUrl\\\"","//$myIP"
        /usr/bin/node /root/yc73pmflsa/vnj8dmelps.js $ipKey $vmessUrl
}
# Manual_detection
update_server
