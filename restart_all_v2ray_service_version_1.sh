#!/bin/bash
/root/ban_iptables.sh banbt
ulimit -n 1000000

/usr/bin/systemctl stop caddy
/usr/bin/systemctl stop v2ray
/usr/bin/systemctl stop xray
/usr/bin/systemctl stop nginx
/usr/bin/systemctl stop trojan-go.service
/usr/bin/systemctl stop snap.shadowsocks-libev.ss-server-daemon.service
sudo pkill -f nginx

/usr/bin/systemctl start caddy
/usr/bin/systemctl start v2ray
/usr/bin/systemctl start xray
/usr/bin/systemctl start nginx
/usr/bin/systemctl start trojan-go.service
/usr/bin/systemctl start snap.shadowsocks-libev.ss-server-daemon.service

/bin/systemctl stop caddy
/bin/systemctl stop v2ray
/bin/systemctl stop xray
/bin/systemctl stop nginx
/bin/systemctl stop trojan-go.service
/bin/systemctl stop snap.shadowsocks-libev.ss-server-daemon.service
sudo pkill -f nginx

/bin/systemctl start caddy
/bin/systemctl start v2ray
/bin/systemctl start xray
/bin/systemctl start nginx
/bin/systemctl start trojan-go.service
/bin/systemctl start snap.shadowsocks-libev.ss-server-daemon.service

ulimit -n
