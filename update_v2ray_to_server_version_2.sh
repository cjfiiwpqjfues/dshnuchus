#!/bin/bash

get_ip(){
    local IP=$( ip addr | egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | egrep -v "^192\.168|^172\.1[6-9]\.|^172\.2[0-9]\.|^172\.3[0-2]\.|^10\.|^127\.|^255\.|^0\." | head -n 1 )
    [ -z ${IP} ] && IP=$( wget -qO- -t1 -T2 ipv4.icanhazip.com )
    [ -z ${IP} ] && IP=$( wget -qO- -t1 -T2 ipinfo.io/ip )
    [ ! -z ${IP} ] && echo ${IP} || echo
}

vmessUrlPre="$(base64 -w 0 /usr/local/vmess_qr.json)"

if [[ -n "$vmessUrlPre" ]]; then
    myIP=$(get_ip)
    ipKey=$myIP
    ipKey=${ipKey//./_}
    ipKey=$ipKey"_vmess"
    vmessUrl="vmess://$vmessUrlPre"
    connectValue="\\\"$vmessUrl\\\"","//$myIP"

    echo $vmessUrl

    curl -X PATCH -d '{ "'$ipKey'": "'$connectValue'"}' \
    'https://jeicmijifirg-default-rtdb.firebaseio.com/connect_list.json'
fi

trojanUrlPre="$(base64 -w 0 /root/trojan-go/server.json)"

if [[ -n "$trojanUrlPre" ]]; then
    myIP=$(get_ip)
    ipKey=$myIP
    ipKey=${ipKey//./_}
    ipKey=$ipKey"_trojan"
    trojanUrl="trojan://$trojanUrlPre"
    connectValue="\\\"$trojanUrl\\\"","//$myIP"

    echo $trojanUrl

    curl -X PATCH -d '{ "'$ipKey'": "'$connectValue'"}' \
    'https://jeicmijifirg-default-rtdb.firebaseio.com/connect_list.json'
fi

vmessUrlPre="$(base64 -w 0 /root/serviceConnectionCode)"

if [[ -n "$vmessUrlPre" ]]; then
    myIP=$(get_ip)
    ipKey=$myIP
    ipKey=${ipKey//./_}
    ipKey=$ipKey"_vmess"
    vmessUrl="vmess://$vmessUrlPre"
    connectValue="\\\"$vmessUrl\\\"","//$myIP"

    echo $vmessUrl

    curl -X PATCH -d '{ "'$ipKey'": "'$connectValue'"}' \
    'https://jeicmijifirg-default-rtdb.firebaseio.com/connect_list.json'
fi

