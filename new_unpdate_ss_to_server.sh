#!/bin/bash
encryptionMethod=$1
serverPassword=$2
ipAddress=$3
networkPort=$4
targetServer=$5

connectValue=ss://$(echo -n "${encryptionMethod}:${serverPassword}@${ipAddress}:${networkPort}" | base64 -w 0)
connectValue="\\\"$connectValue\\\"","//${ipAddress}"

ipKey=${ipAddress//./_}

curl -X PATCH -d '{ "'$ipKey'": "'$connectValue'"}' \
'https://'$targetServer'.firebaseio.com/connect_list.json'
