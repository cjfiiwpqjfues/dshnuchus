var fs = require('fs');
const {
    v4: uuidv4
} = require('uuid');

var configJsonPath = "/etc/v2ray/config.json";

var vmessQrJsonPath = "/usr/local/vmess_qr.json";

var nginxPath = "/etc/nginx/conf/conf.d/v2ray.conf";

var port = 80;
// var port = getRandomPort();
var uuid = uuidv4();

fs.readFile(configJsonPath, function(error1, data1) {
    if (error1) {
        console.log('fail');
    } else {
        var jsonConfig = JSON.parse(data1.toString());

        var v2rayPort = jsonConfig.inbounds[0].port;

        // while (port === v2rayPort) {
        //     port = getRandomPort();
        // }

        jsonConfig.inbounds[0].settings.clients[0].id = uuid;
        // jsonConfig.inbounds[0].port = port;
        fs.writeFile(configJsonPath, JSON.stringify(jsonConfig, null, 4), function(error) {

        })

        fs.readFile(vmessQrJsonPath, function(error2, data2) {
            if (error2) {
                console.log('fail');
            } else {
                var jsonVmessQr = JSON.parse(data2.toString());
                jsonVmessQr.id = uuid;
                jsonVmessQr.port = port.toString();
                fs.writeFile(vmessQrJsonPath, JSON.stringify(jsonVmessQr, null, 4), function(error) {

                })
            }
        });

        fs.readFile(nginxPath, function(error2, data2) {
            if (error2) {
                console.log('fail');
            } else {
                var nginxConfig = data2.toString();
                nginxConfig = nginxConfig.replace(/listen [0-9]+ ssl http2;/,'listen '+port+' ssl http2;');
                nginxConfig = nginxConfig.replace(/listen \[::]:[0-9]+ http2;/,'listen [::]:'+port+' http2;');
                fs.writeFile(nginxPath, nginxConfig, function(error) {

                })
            }
        });
    }
});

function getRandomPort() {
    var rate = Math.random();
    rate = Math.floor(rate * (65535 - 40001 + 1)) + 40001;
    return rate;
}
