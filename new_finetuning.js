var fs = require('fs');
var targetPath = "/usr/local/etc/xray/config.json";

getJsonConfig();

function getJsonConfig() {

    fs.readFile(targetPath, function(error, data) {
        if (error) {
            console.log('讀取失敗');
        } else {
            setJsonConfig(data.toString());
        }
    });

}

function setJsonConfig(rawJsonConfig) {

    var jsonConfig = JSON.parse(rawJsonConfig);

    // routing
    var routing = {
        "domainStrategy": "AsIs",
        "rules": []
    }

    jsonConfig.routing = routing;

    var args = process.argv;

    // outbounds
    var outbounds = null;

    if (args[2] != undefined){
        outbounds = [{
        "sendThrough":args[2],
        "protocol": "freedom"
    }]
    }else{
        outbounds = [{
        "protocol": "freedom"
    }]
    }

    jsonConfig.outbounds = outbounds;

    // outbounds
    var outboundsBlocked = {
        "protocol": "blackhole",
        "settings": {

        },
        "tag": "blocked"
    }

    jsonConfig.outbounds.push(outboundsBlocked);

    // BT
    var banBT = {
        "type": "field",
        "outboundTag": "blocked",
        "protocol": [
            "bittorrent"
        ]
    }

    jsonConfig.routing.rules.unshift(banBT);


    // ----------------------------------------

    // // BanDomain
    // var banDomain = {
    //     "type": "field",
    //     "outboundTag": "blocked",
    //     "domain": [
    //         "domain:speedtest.net",
    //         "domain:speedtest.cn",
    //         "domain:ip.sb",
    //         "domain:skk.moe",
    //         "domain:whoer.net",
    //         "domain:whatismyipaddress.com",
    //         "domain:ez2o.com",
    //         "domain:whatismyip.com",
    //         "domain:geoipview.com",
    //         "domain:ifreesite.com",
    //         "domain:nkuht.edu",
    //         "domain:myip.com",
    //         "domain:j4.com",
    //         "domain:ip-api.com",
    //         "domain:ipapi.co",
    //         "domain:ipstack.com",
    //         "domain:iplocation.net",
    //         "domain:expressvpn.com",
    //         "domain:kinsta.com",
    //         "domain:nordvpn.com",
    //         "domain:whatismybrowser.com",
    //         "domain:ip2location.com",
    //         "domain:whatsmyip.org",
    //         "domain:ipip.net",
    //         "domain:ip.cn",
    //         "domain:chinaz.com",
    //         "domain:ip138.com",
    //         "domain:baidu.com",
    //         "domain:bdstatic.com",
    //         "domain:bcebos.com"
    //     ],
    // }

    // jsonConfig.routing.rules.unshift(banDomain);
    // // ----------------------------------------

    writeJsonConfig(JSON.stringify(jsonConfig, null, 4));
}

function writeJsonConfig(finalJsonConfig) {

    fs.writeFile(targetPath, finalJsonConfig, function(error) {

        if (error) {
            console.log('寫入失敗');
        } else {
            console.log('寫入成功');
        }

    });

}
