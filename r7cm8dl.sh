#!/bin/bash

serviceConnectionCodePath="/root/serviceConnectionCode"
serviceConfigPath="/usr/local/etc/xray/config"

ip=$(curl -s https://ipinfo.io/ip)
	[[ -z $ip ]] && ip=$(curl -s https://api.ip.sb/ip)
	[[ -z $ip ]] && ip=$(curl -s https://api.ipify.org)
	[[ -z $ip ]] && ip=$(curl -s https://ip.seeip.org)
	[[ -z $ip ]] && ip=$(curl -s https://ifconfig.co/ip)
	[[ -z $ip ]] && ip=$(curl -s https://api.myip.com | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}")
	[[ -z $ip ]] && ip=$(curl -s icanhazip.com)
	[[ -z $ip ]] && ip=$(curl -s myip.ipip.net | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}")

uuid=$(cat /proc/sys/kernel/random/uuid)
port=${1}

sed -i 's/1111/'${port}'/g' ${serviceConfigPath}
sed -i 's/2222/'${uuid}'/g' ${serviceConfigPath}
sed -i 's/3333/'${ip}'/g' ${serviceConfigPath}

ip=\"${ip}\"
uuid=\"${uuid}\"
port=\"${port}\"

# echo -n "{\"v\":\"2\",\"ps\":${ip},\"add\":${ip},\"port\":${port},\"id\":${uuid},\"aid\":\"0\",\"net\":\"kcp\",\"type\":\"none\",\"host\":\"\",\"path\":\"\",\"tls\":\"\",\"type\":\"utp\"}" > ${serviceConnectionCodePath}

echo -n "{\"v\":\"2\",\"ps\":${ip},\"add\":${ip},\"port\":${port},\"id\":${uuid},\"aid\":\"1000\",\"net\":\"ws\",\"type\":\"none\",\"host\":\"\",\"path\":\"\",\"tls\":\"\"}" > ${serviceConnectionCodePath}
