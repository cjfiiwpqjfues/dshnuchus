var fs = require('fs');
var targetPath = "/etc/v2ray-agent/xray/conf/04_VMess_TCP_inbounds.json";
var finalPath = "/usr/local/vmess_qr.json";

getConfig();

function getConfig() {

    fs.readFile(targetPath, function(error, data) {
        if (error) {
            console.log('讀取失敗');
        } else {
            setConfig(data.toString());
        }
    });

}

function setConfig(rawConfig) {

    var config = JSON.parse(rawConfig);

    var newConfig = {
        "port": "443",
        "ps": config.inbounds[0].settings.clients[0].email.replace('_vmess_tcp', ''),
        "tls": "tls",
        "id": config.inbounds[0].settings.clients[0].id,
        "aid": "1",
        "v": "2",
        "host": config.inbounds[0].settings.clients[0].email.replace('_vmess_tcp', ''),
        "type": "http",
        "path": config.inbounds[0].streamSettings.tcpSettings.header.request.path[0],
        "net": "tcp",
        "add": config.inbounds[0].settings.clients[0].email.replace('_vmess_tcp', ''),
        "allowInsecure": 0,
        "method": "none",
        "peer": config.inbounds[0].settings.clients[0].email.replace('_vmess_tcp', ''),
        "obfs": "http",
        "obfsParam": config.inbounds[0].settings.clients[0].email.replace('_vmess_tcp', '')
    }

    writeConfig(JSON.stringify(newConfig, null, 4));
}

function writeConfig(finalConfig) {

    fs.writeFile(finalPath, finalConfig, function(error) {

        if (error) {
            console.log('寫入失敗');
        } else {
            console.log('寫入成功')
        }

    });

}
